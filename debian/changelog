foodcritic (13.1.1-3) UNRELEASED; urgency=medium

  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 03:04:43 +0530

foodcritic (13.1.1-2) unstable; urgency=medium

  * Build-Depend on ronn, rather than ruby-ronn (Closes: #903074)
  * Declare Rules-Requires-Root: no.
  * Bump to Standards-Version 4.1.5, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 22 Jul 2018 21:42:51 +0800

foodcritic (13.1.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #892188)
  * Refresh patches.
  * Bump copyright years.
  * Bump debhelper compat to 10.
    - Drop dh_installchangelogs override, no longer needed.
  * Bump Standards-Version to 4.1.4, no changes needed.
  * Migrate package to salsa.debian.org.
  * (Build-)Depend on ruby-ffi-yajl rathe rthan ruby-yajl - upstream switched.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 19 May 2018 22:52:41 +0200

foodcritic (8.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Use SOURCE_DATE_EPOCH in the generated manpage.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 10 Nov 2016 11:54:06 +0000

foodcritic (7.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.

 -- Stefano Rivera <stefanor@debian.org>  Sat, 20 Aug 2016 09:50:33 -0700

foodcritic (7.0.1-2) unstable; urgency=medium

  * Use ruby-hashery instead of rufus-lru. It is compatible, and in the
    archive already.

 -- Stefano Rivera <stefanor@debian.org>  Mon, 25 Jul 2016 13:16:15 -0700

foodcritic (7.0.1-1) unstable; urgency=medium

  * New upstream release.
  * No need to repack, any more.
  * Refresh patches.
  * Drop dont-load-minitest-autorun-in-cucumber-tests.patch and no-rubygems,
    superseded upstream.
  * B-D on ruby-cucumber-core instead of ruby-gherkin, following upstream.
  * Bump Standards-Version to 3.9.8, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Mon, 25 Jul 2016 12:18:22 -0700

foodcritic (6.0.1+ds-1) unstable; urgency=medium

  * New upstream point release.
  * Repack upstream source, to avoid coverage output (including minified js).
  * Refresh patches.
  * Bump Standards-Version to 3.9.7, no changes needed.

 -- Stefano Rivera <stefanor@debian.org>  Sun, 27 Mar 2016 22:24:42 -0400

foodcritic (6.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Bump copyright years.
  * Use https://anonscm.../git/... for both Vcs URLs.

 -- Stefano Rivera <stefanor@debian.org>  Tue, 02 Feb 2016 14:21:27 +1100

foodcritic (5.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright file.
  * Refresh patches.
  * Drop no-coverage and FC010-broken patches, superseded upstream.
  * Point Vcs-Browser at https cgit.
  * Bump debhelper compat level to 9.
  * Use https in the watch file.
  * Use an absolute require in the spec file, so the autopkgtest can run
    against the system install.
  * Avoid depending on the lib directory in tests, for autopkgtests.
  * Install treetop grammar file.
  * Update homepage to foodcritic.io (Closes: #809653)

 -- Stefano Rivera <stefanor@debian.org>  Sun, 03 Jan 2016 20:11:56 +0200

foodcritic (4.0.0-2) unstable; urgency=medium

  * Team upload.
  * dont-load-minitest-autorun-in-cucumber-tests.patch: avoid
    loading minitest autorunnner from cucumber tests (Closes: #795617)

 -- Antonio Terceiro <terceiro@debian.org>  Tue, 25 Aug 2015 16:26:10 -0300

foodcritic (4.0.0-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches.
  * Bump Standards-Version to 3.9.6, no changes needed.
  * Bump copyright years.
  * Patch no-rufus-lru: We haven't packaged ruby-rufus-lru, yet, so revert
    back to a persistent cache for now.

 -- Stefano Rivera <stefanor@debian.org>  Fri, 24 Oct 2014 01:02:29 -0700

foodcritic (3.0.3-1) unstable; urgency=low

  * New upstream release.
  * Refresh patches.
  * Drop rak-free patch, superseded upstream.
  * Drop gherkin-2.11.1 patch, and bump gherkin dependency to 2.11.7.
  * Switch to stable tarballs from gemwatch.
  * Update copyright years.
  * FC010 is currently broken due to changes in Chef. Ignore the test failures.
  * Ruby 1.8 is no longer supported in Debian, so declare XS-Ruby-Versions:
    all.
  * Bump Standards-Version to 3.9.5, no changes needed.
  * The manpage is now included upstream.

 -- Stefano Rivera <stefanor@debian.org>  Thu, 16 Jan 2014 14:49:30 +0200

foodcritic (2.1.0-1) unstable; urgency=low

  * Initial release (Closes: #711220)

 -- Stefano Rivera <stefanor@debian.org>  Tue, 11 Jun 2013 00:34:40 +0200
